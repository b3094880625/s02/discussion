import pandas as pd

class Graph:
	def __init__(self, nodes, directed=True):
		self.m_directed = directed
		self.m_adj_list = {node: list() for node in nodes}

	def add_edge(self, node1, node2, weight=1):
		self.m_adj_list[node1].append((node2, weight))

		if not self.m_directed:
			if node1 == node2:
				pass
			else:
				self.m_adj_list[node2].append((node1, weight))

	def print_adj_list(self):
		for key in self.m_adj_list.keys():
			print("node", key, ": ", self.m_adj_list[key])

n = [0,1,2,3,4]

graph = Graph(n, directed=False)

graph.add_edge(0, 2, 3)
graph.add_edge(0, 0, 25)
graph.add_edge(0, 1, 5)
graph.add_edge(1, 3, 1)
graph.add_edge(1, 4, 15)
graph.add_edge(4, 2, 7)
graph.add_edge(4, 3, 11)

graph.print_adj_list()